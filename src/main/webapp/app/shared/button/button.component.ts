import { Component, Input } from '@angular/core';

@Component({
    selector: 'jhi-button',
    template: `
    <button [disabled]="disabled" [type]="type" [class]="getButtonClass()">
      <ng-content></ng-content>
    </button>
  `,
    styleUrls: ['./button.component.scss']
})
export class ButtonComponent {
    @Input() type = 'button';
    @Input() disabled = false;
    @Input() status = ButtonType.PRIMARY;

    getButtonClass() {
        switch (this.status) {
            case ButtonType.ERROR:
                return 'button-error';
            case ButtonType.SUCCESS:
                return 'button-success';
            default:
                return;
        }
    }
}

export enum ButtonType {
    SUCCESS = 'success',
    ERROR = 'error',
    PRIMARY = 'primary'
}
