import { NgModule } from '@angular/core';

import { DemoSharedLibsModule, FindLanguageFromKeyPipe, JhiAlertComponent, JhiAlertErrorComponent } from './';
import {ButtonComponent} from './button/button.component';

@NgModule({
    imports: [DemoSharedLibsModule],
    declarations: [FindLanguageFromKeyPipe, ButtonComponent, JhiAlertComponent, JhiAlertErrorComponent],
    exports: [DemoSharedLibsModule, ButtonComponent, FindLanguageFromKeyPipe, JhiAlertComponent, JhiAlertErrorComponent]
})
export class DemoSharedCommonModule {}
